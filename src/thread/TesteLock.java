package thread;

import java.util.Random;

import lock.Lock;


public class TesteLock extends Thread{
	private int myId;
	private Lock lock;
	private Random r = new Random();
	public void run() {
		while (true) {
		this.lock.requestCS(myId);
		CriticalSection();
		lock.releaseCS(myId);
		nonCriticalSection();
		}
	}
	public TesteLock(int myId, Lock lock) {
		super();
		this.myId = myId;
		this.lock = lock;
	}
	private void nonCriticalSection() {
		System.out.println(myId +
		" n�o est� na CS");
		mySleep(r.nextInt(1000));
	}
	private void CriticalSection() {
		System.out.println(myId +
		" entrou na CS *****");
		mySleep(r.nextInt(1000));
		System.out.println(myId +
		" saiu da CS *****");
	}
	public void mySleep(int time) {
		
	}
}
